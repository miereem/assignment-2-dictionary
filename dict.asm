%define SIZE 8
extern string_equals

global find_word

section .text

; Функция ищёт ключ в словаре и, если найден, то возвращает адрес начала, иначе вернёт 0.
; Функция принимает указатель на нуль-терминированную строку и указаткль на начало словаря.
; rdi - указатель на строку
; rsi - указатель на начало словаря
; rax - ноль или начало строки-значения
find_word:
    .loop:
        add rsi, SIZE ; указатель на следующий элемент
        push rdi
        push rsi
        call string_equals
        pop rsi
        pop rdi
        cmp rax, 1
        je .key_found ; ключ найден
        mov rsi, [rsi-SIZE] ; указатель на следующий элемент
        cmp rsi, 0
        jne .loop ; продолжить поиск
        jmp .end

    .key_found:
        sub rsi, SIZE
        mov rax, rsi
        ret

    .end:
        xor rax, rax
        ret
