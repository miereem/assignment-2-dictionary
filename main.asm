%include 'words.inc'
%include 'lib.inc'

extern find_word

%define ENDL 0xA
%define BUFFER_SIZE 256
%define SIZE 8

section .rodata
    input_key: db 'Enter key:', ENDL, 0
    long_key: db 'Invalid key size', ENDL, 0
    success: db 'Definition:', ENDL, 0
    no_key: db 'Key not found', ENDL, 0

section .bss
    buffer: resb BUFFER_SIZE

global _start

section .text

_start: 
        mov rdi, input_key
        call print_string

        mov rdi, buffer
        mov rsi, BUFFER_SIZE
        call read_word

        cmp rax, 0 ; Проверить, если в rax 0, то прочитать не удалось
        je .print_long_key

        push rdx ; Сохраняем длину ключа, чтобы потом её скипнуть
        mov rdi, buffer ; Ложим ключ
        mov rsi, top ; Ложим конец словаря
        call find_word

        cmp rax, 0
        je .print_no_key

        pop rdx
        add rax, SIZE
        add rax, rdx
        inc rax
        push rax ; Сохраняем адрес начала значения

        mov rdi, success
        call print_string
        pop rdi
        call print_string
        call print_newline
        xor rdi, rdi
        call exit

    .print_no_key:
        mov rdi, no_key
        call print_err
        jmp .end_err

    .print_long_key:
        mov rdi, long_key
        call print_err
        jmp .end_err

    .end_err:
        mov rdi, 1
        call exit
